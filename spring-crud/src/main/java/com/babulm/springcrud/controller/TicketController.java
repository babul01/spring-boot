package com.babulm.springcrud.controller;

import com.babulm.springcrud.entity.Ticket;
import com.babulm.springcrud.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TicketController {

    @Autowired
    private TicketRepository ticketRepository;

    @PostMapping("/book-tickets")
    public String bookTickets(@RequestBody List<Ticket> ticketList) {
        ticketRepository.saveAll(ticketList);
        return "Tickets have booked: "+ticketList.size();
    }

    @GetMapping("/tickets")
    public List<Ticket> getTickets() {
        return ticketRepository.findAll();
    }

    @PostMapping("/book-ticket")
    public String bookTicket(@RequestBody Ticket ticket) throws Exception {
        try {
            if (ticket.getId() != null) {
                Optional<Ticket> existTicket = ticketRepository.findById(ticket.getId());
                if (existTicket.isPresent()) {
                    throw new Exception("Ticket is already exist");
                }
            }
            ticketRepository.save(ticket);
            return "Ticket has been booked";
        }
        catch (Exception e) {
            throw new Exception("Ticket booking is failed reason :"+e.getMessage());
        }
    }
}
