package com.babulm.springcrud.repository;

import com.babulm.springcrud.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}
